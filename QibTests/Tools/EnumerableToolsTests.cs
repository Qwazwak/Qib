using Qib.Extensions;
using Qib.Tools;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace QLibTests.Tools
{
    public class EnumerableToolsTests
    {
        [Fact]
        public void ToEnumerable()
        {
            Assert.NotEmpty(EnumerableTools.ToEnumerable(new string[] { "Hello", "Testing" }));
            Assert.IsAssignableFrom<IEnumerable<string>>(EnumerableTools.ToEnumerable(new string[] { "Hello", "Testing" }));
        }

        [Fact]
        public void ToEnumerable_Null()
        {
            Assert.Empty(EnumerableTools.ToEnumerable((int[])(null!)));
            Assert.IsAssignableFrom<IEnumerable<int>>(EnumerableTools.ToEnumerable((int[])(null!)));
        }

        [Fact]
        public void ToEnumerable_empty()
        {
            Assert.Empty(EnumerableTools.ToEnumerable(System.Array.Empty<int>()));
            Assert.IsAssignableFrom<IEnumerable<int>>(EnumerableTools.ToEnumerable(System.Array.Empty<int>()));
        }

        [Theory]
        [InlineData(0, 1, 50000)]
        [InlineData(1 * 10000, 1 * 10000, 1000 * 10000)]
        public void EnumerableDomain(int Start, int IncreaseBy, int End)
        {
            IEnumerable<int> Domain = EnumerableTools.Domain(Start, IncreaseBy, End);
            if (Domain.Any())
            {
                Assert.Equal(Start, Domain.First());
                Assert.True(End >= Domain.Last());
            }
            Parallel.ForEach(Domain.WithIndex(),
                p => Assert.Equal(Start + (p.Index * IncreaseBy), p.Value));
        }

        public class EnumerableToolsTests_DomainData : TheoryData<(int Start, int IncreaseBy, int End)[]>
        {
            public EnumerableToolsTests_DomainData()
            {
                IEnumerable<int> StartingValues = EnumerableTools.ToEnumerable(0, 1, 2, 5, 10, 8, 13, 50);
                IEnumerable<int> IncreaseByValues = EnumerableTools.ToEnumerable(1, 2, 3, 5, 4, 21, 50);
                IEnumerable<int> TotalValues = EnumerableTools.ToEnumerable(1, 2, 5, 7, 16, 32, 38, 78, 500);
                IEnumerable<(int s, int i, int e)> Configs = StartingValues.SelectMany(s => TotalValues.SelectMany(c => IncreaseByValues.Select(i => (IncreaseBy: i, End: s + (c * i)))))
                    .SelectMany(p => StartingValues.Select(s => (Start: s, p.IncreaseBy, p.End)))
                    .Where(set => set.Start < set.End);
                 Parallel.ForEach(Configs.Take(10000).Chunk(50), g => Add(g));
            }
        }
    }
}