using Qib;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace QLibTests
{
    public class QMathTests
    {
        const int TestsToGenerate = 1000;

        [Fact]
        public void OddNumbersOdd() => Parallel.ForEach(Enumerable.Range(1, 5000).Select(i => (i * 2) - 1), o => Assert.True(QMath.IsOdd(o)));
        [Fact]
        public void EvenNumbersEven() => Parallel.ForEach(Enumerable.Range(0, 5000).Select(i => i * 2), o => Assert.True(QMath.IsEven(o)));

        [Theory]
        [InlineData(0, 1)]
        [InlineData(-1, 1)]
        [InlineData(1, 1)]
        public void CountInt(int Number, int Count) => Assert.Equal(QMath.CountDigits(Number), Count);

        [Fact]
        public void CountInt_Bulk()
        {
            Random rand = new(123567);
            Parallel.For(0, TestsToGenerate, _ =>
            {
                int value = rand.Next(int.MinValue, int.MaxValue);
                Assert.Equal(QMath.CountDigits(value), value.ToString().TrimStart('-').Length);
            });
        }

        [Theory]
        [InlineData(0, 1)]
        [InlineData(-1, 1)]
        [InlineData(1, 1)]
        public void Countlong(long Number, int Count) => Assert.Equal(QMath.CountDigits(Number), Count);

        [Fact]
        public void Countlong_Bulk()
        {
            Random rand = new(584268);
            Parallel.For(0, TestsToGenerate, _ =>
            {
                long value = rand.NextInt64(long.MinValue, long.MaxValue);
                Assert.Equal(QMath.CountDigits(value), value.ToString().TrimStart('-').Length);
            });
        }
    }
}