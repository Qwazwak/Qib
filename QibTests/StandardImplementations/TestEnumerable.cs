﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace QLibTests.Extensions.IEnumerableExtensions
{
    internal class TestEnumerable<T> : IEnumerable<T>
    {
        internal ICollection<T> Items;
        public TestEnumerable(ICollection<T> Items) => this.Items = Items;
        public TestEnumerable(IEnumerable<T> Items) => this.Items = Items.ToList();
        public TestEnumerable(params T[] Items) => this.Items = Items;

        public IEnumerator<T> GetEnumerator() => Items.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();
    }
}