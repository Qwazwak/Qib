using Qib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace QLibTests.Extensions.StringExtensions
{
    public class StringWhitespaceEmptyTests
    {
        [Theory]
        [ClassData(typeof(StringExtensionTests_Data))]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "xUnit1026:Theory methods should use all of their parameters", Justification = "ToSeperateBlankLineTests")]
        public void IsEmpty(int _, string? Content) => Assert.Equal(Content != null && string.IsNullOrEmpty(Content), Content.IsEmpty());

        [Theory]
        [ClassData(typeof(StringExtensionTests_Data))]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "xUnit1026:Theory methods should use all of their parameters", Justification = "ToSeperateBlankLineTests")]
        public void IsWhitespace(int _, string? Content) => Assert.Equal(!string.IsNullOrEmpty(Content) && string.IsNullOrWhiteSpace(Content), Content.IsWhitespace());
    }
    public class StringExtensionTests_Data : TheoryData<int, string?>
    {
        public StringExtensionTests_Data()
        {
            Random rng = new(1245);
            List<string?> Strings = new()
        {
            null,
            "",
            "  ",
            "       ",
            "     +  ",
            "   \\\\\\n    ",
            ".\n\t\n",
            "HelloWorld",
            "Some more text",
            "Some more text",
            "\n\t\n",
            "\r",
            "Bacon ipsum dolor amet turducken ham hock salami sirloin ground round tail pork leberkas meatball sausage rump. Pork loin pancetta picanha sirloin drumstick. Doner capicola pork loin pork chop shoulder cow t-bone pancetta filet mignon turkey short loin. Sirloin venison pork chop, prosciutto doner cow salami chislic pastrami chuck beef burgdoggen shoulder meatloaf ground round.",
            "Shank corned beef kielbasa bacon kevin. Pork landjaeger andouille prosciutto beef pastrami. Flank chislic bacon landjaeger. Ham kielbasa drumstick cow spare ribs pork loin jowl ball tip.Strip steak t - bone chicken, bresaola alcatra andouille biltong tail drumstick ham sirloin porchetta cow pork.Filet mignon tenderloin flank shoulder kevin pork belly. Ground round ribeye pork loin, frankfurter landjaeger tenderloin kevin leberkas burgdoggen jowl.",
            "Meatloaf boudin pancetta fatback doner, bresaola tongue. Pig prosciutto landjaeger hamburger tail, short ribs flank brisket. Chuck short loin pastrami tail ball tip tri - tip turducken pork hamburger porchetta landjaeger alcatra chicken shank cow.Chislic hamburger boudin rump salami fatback pork belly shank chuck pig frankfurter pork loin. Meatloaf kevin pig doner. Pastrami shank kevin ground round drumstick, sausage tri - tip.Ham ham hock frankfurter doner pig, boudin swine.",
            "Strip steak pork short loin burgdoggen fatback. Shankle meatball spare ribs boudin ham hock corned beef flank short loin chicken ground round pastrami venison.Meatloaf pastrami prosciutto, strip steak shankle biltong t - bone hamburger pig tenderloin buffalo chuck leberkas sirloin.Leberkas hamburger shank, jowl capicola fatback meatball tongue brisket tenderloin porchetta chicken biltong meatloaf corned beef.Tri - tip pancetta brisket pig prosciutto. Prosciutto hamburger picanha, beef ribs corned beef cow ground round turkey pastrami.",
            "Leberkas beef pork chop fatback, pastrami swine ham hock. Alcatra beef spare ribs brisket, jowl pig andouille frankfurter flank buffalo pork salami pork chop. Tongue frankfurter ground round, beef landjaeger leberkas tenderloin venison strip steak. Cow rump bresaola kielbasa ham hock doner picanha pork loin chicken beef alcatra tenderloin shoulder short ribs corned beef.",
            string.Concat(Enumerable.Range(0, 10000).Select(_ => rng.Next(1, 10).ToString())),
        };
            Strings.Shuffle(51256);
            foreach ((int Index, string Value) in Strings.WithIndex())
                Add(Index, Value);
        }
    }
}