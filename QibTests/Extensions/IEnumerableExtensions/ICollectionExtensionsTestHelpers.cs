﻿using Qib.Extensions;
using QLibTests.Extensions.IEnumerableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QLibTests.Extensions.ICollectionExtensionsTests
{
    internal static class ICollectionExtensionsTestHelpers
    {
        // All of these transforms should take an enumerable and produce another enumerable with the same contents.
        private static List<Func<ICollection<T>, ICollection<T>>> IdentityTransformsInner<T>()
            => new()
            {
                e => e,
                e => e.ToArray(),
                e => e.ToList(),
                e => new TestGenericCollection<T>(e),
            };

        internal static IEnumerable<IEnumerable<Func<ICollection<T>, ICollection<T>>>> IdentityTransformsPowerset<T>() => IdentityTransformsInner<T>().Powerset();
        internal static IEnumerable<ICollection<T>> IdentityTransformsPowersetInvoke<T>(ICollection<T> Source) => IdentityTransformsPowerset<T>().Select(i => i.Aggregate(Source, (s, n) => s = n.Invoke(s)));
        internal static IEnumerable<IEnumerable<ICollection<T>>> IdentityTransformsPowersetInvoke<T>(IEnumerable<ICollection<T>> Sources) => Sources.Select(s => IdentityTransformsPowersetInvoke(s));
    }
}