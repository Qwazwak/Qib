using Qib.Extensions;
using System;
using System.Linq;
using Xunit;

namespace QLibTests.Extensions
{
    public class EnumExtensionTests
    {
        private enum Alpha
        {
            Zero,
            One,
            Two,
            Three,
            Four,
            Five,
            fdggsdffgds,
            gfdssdfggdfs,
            fgdsfgdsgfds,
            gfdssgdfdgfs,
            sdfggfdsgdfs,
            sdfsdffg,
            bvbvccbv,
            Final
        }

        [Fact]
        public void ModWithTest()
        {
            int EnumLength = Enum.GetValues<Alpha>().Length;
            const int RepeatTimes = 60;
            foreach ((int IntValue, Alpha EnumValue) in Enumerable.Range(0, EnumLength * RepeatTimes).ModWithEnum<int, Alpha>())
            {
                Assert.Equal(EnumValue, (Alpha)(IntValue % EnumLength));
            }
        }
    }
}