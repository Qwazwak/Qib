﻿using Qib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QLibTests.StandardImplementation
{
    internal static class IEnumerableExtensionTestHelpers
    {
        internal static IEnumerable<T> ForceNotCollection<T>(IEnumerable<T> source)
        {
            foreach (T item in source) yield return item;
        }

        // All of these transforms should take an enumerable and produce another enumerable with the same contents.
        private static List<Func<IEnumerable<T>, IEnumerable<T>>> IdentityTransformsInner<T>()
            => new()
            {
                e => e,
                e => e.ToArray(),
                e => e.ToList(),
                e => e.Select(i => i),
                e => e.Concat(Array.Empty<T>()),
                e => ForceNotCollection(e),
                e => e.Skip(0),
                e => e.Concat(ForceNotCollection(Array.Empty<T>())),
                e => e.Where(_ => true),
            };

        internal static IEnumerable<IEnumerable<Func<IEnumerable<T>, IEnumerable<T>>>> IdentityTransformsPowerset<T>() => IdentityTransformsInner<T>().Powerset();
    }
}