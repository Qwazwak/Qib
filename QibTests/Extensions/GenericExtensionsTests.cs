﻿using Qib.Extensions;
using System;
using System.Linq;
using Xunit;

namespace QLibTests.Extensions
{
    public class GenericExtensionsTests
    {
        [Theory]
        [InlineData(1, new int[] { 0, 5, 1, 1, 2, 45 })]
        [InlineData("TheKey", new string[] { "nope", "nothere", "almost", "TheKey", "TooFar" })]
        public void EqualsAnyTrueTests<T>(T Item, Array Others) => Assert.True(Item.EqualsAny((T[])Others));

        [Theory]
        [InlineData(1, new int[] { 0, 5, 2, 45 })]
        [InlineData("TheKey", new string[] { "nope", "nothere", "almost", "NotTheKey", "TooFar" })]
        public void EqualsAnyFalseTests<T>(T Item, Array Others) => Assert.False(Item.EqualsAny((T[])Others));
    }
}