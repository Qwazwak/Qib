### About the project
Qib is a collection of Extentions, Wrappers, and tools

### Development
Branches should be named by 'category/StoryID-description'
Categories are:
  - feature
  - hotfix
  - maintenance
  - documentation

### Building package
1. Run the tests and make sure everything is okay
   - Make sure that you added tests for any new additions!
2. With visual studio right click the project Qib and select "Pack"
3. Thats all I know so far!

### About the Project
I started this once I wrote the same extentions three times for personal projects and work, so if I am rewriting this and repacking snippets from online, why not package it all together for sharing and testing!


The name Qib comes from my original name idea "QLib" (For QwazwakLibrary), but there was already a nuget package with the name, so Qib it is!

Licensed under The Hippocratic License 3.0 AND GNU LGPL v3.0