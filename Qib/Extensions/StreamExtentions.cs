﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Qib.Extensions
{
    public static class StreamExtensions
    {
        private const int ReadAllBytesDefaultBlockSize = 64;
        public static IAsyncEnumerable<byte> ReadAllBytesFlatAsync(this Stream Stream, int BlockSize = ReadAllBytesDefaultBlockSize) => Stream.ReadAllBytesAsync(BlockSize).SelectMany(b => b.ToAsyncEnumerable());
        public static async IAsyncEnumerable<IEnumerable<byte>> ReadAllBytesAsync(this Stream Stream, int BlockSize = ReadAllBytesDefaultBlockSize)
        {
            byte[] Buffer = new byte[BlockSize];
            int BytesInCurrentTaking = await Stream.ReadAsync(Buffer).ConfigureAwait(false);
            while (BytesInCurrentTaking != 0)
            {
                yield return Buffer.Take(BytesInCurrentTaking);
                BytesInCurrentTaking = await Stream.ReadAsync(Buffer).ConfigureAwait(false);
            }
        }

        public static async IAsyncEnumerable<string> ReadAllLinesAsync(this Stream Stream)
        {
            using StreamReader Reader = new(Stream);
            string? CurrentLine = await Reader.ReadLineAsync().ConfigureAwait(false);
            while (CurrentLine != null && !Reader.EndOfStream)
            {
                yield return CurrentLine;
                CurrentLine = await Reader.ReadLineAsync().ConfigureAwait(false);
            }
        }

        public static async IAsyncEnumerable<string> ReadAllLinesAsync(this StreamReader Reader)
        {
            string? CurrentLine = await Reader.ReadLineAsync().ConfigureAwait(false);
            while (CurrentLine != null && !Reader.EndOfStream)
            {
                yield return CurrentLine;
                CurrentLine = await Reader.ReadLineAsync().ConfigureAwait(false);
            }
        }
    }
}