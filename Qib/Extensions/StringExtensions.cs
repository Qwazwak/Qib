﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qib.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveString(this string Source, string ToRemove) => Source.Replace(ToRemove, string.Empty);
        public static string RemoveChars(this string Source, char ToRemove) => Source.RemoveString(char.ToString(ToRemove));

        public static bool IsEmpty(this string? Source) => string.IsNullOrEmpty(Source) && Source != null;
        public static bool IsWhitespace(this string? Source, bool IsEmptyOK = true) => string.IsNullOrWhiteSpace(Source) && Source != null && Source != string.Empty && (IsEmptyOK || Source?.Length == 0);
        /*
        public static IEnumerable<ReadOnlyMemory<char>> SplitViewEvery(this string Source, int CharsInChunk)
        {
            ArgumentNullException.ThrowIfNull(Source, nameof(Source));
            if (CharsInChunk <= 0)
                throw new ArgumentException("Part length has to be positive.", nameof(CharsInChunk));

            //return EnumerableTools.Domain(0, CharsInChunk, Source.Length).Select(i => Source.AsMemory().Slice(i, Math.Min(Source.Length - i, CharsInChunk)));
            for (int i = 0; i < Source.Length; i += CharsInChunk)
                yield return Source.AsMemory().Slice(i, Math.Min(Source.Length - i, CharsInChunk));
        }

        public static IEnumerable<string> SplitEvery(this string Source, int CharsInChunk) => Source.Chunk(CharsInChunk).Select(arr => new string (arr));
    */
    }
}