﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qib.Extensions
{
    public static class IDictionaryExtensions
    {
        internal static Func<IEnumerable<TValue>, TContainer> InitContainer<TValue, TContainer>() where TContainer : ICollection<TValue>, new() => v => { TContainer m = new(); m.AddRange(v); return m; };

        public static void Add<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, TKey Key, TValue Value, Func<TValue, TContainer> NewFactory) where TKey : notnull where TContainer : ICollection<TValue>
        {
            switch (Dictionary)
            {
                case ConcurrentDictionary<TKey, TContainer> Concurrent:
                    Concurrent.AddOrUpdate(Key, _ => NewFactory.Invoke(Value), (_, Collection) => { Collection.Add(Value); return Collection; });
                    break;
                default:
                    if (Dictionary.TryGetValue(Key, out TContainer? Container))
                        Container.Add(Value);
                    else
                        Dictionary[Key] = NewFactory.Invoke(Value);
                    break;
            }
        }

        public static void Add<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, TKey Key, TValue Value) where TKey : notnull where TContainer : ICollection<TValue>, new() => Dictionary.Add(Key, Value, v => new() { v });

        public static void AddValues<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, TKey Key, IEnumerable<TValue> Values, Func<IEnumerable<TValue>, TContainer> NewFactory) where TKey : notnull where TContainer : ICollection<TValue>
        {
            if (Dictionary is ConcurrentDictionary<TKey, TContainer> Concurrent)
                Concurrent.AddOrUpdate(Key, _ => NewFactory.Invoke(Values), (_, Collection) => { Collection.AddRange(Values); return Collection; });
            else if (Dictionary.TryGetValue(Key, out TContainer? Container))
                Container.AddRange(Values);
            else
                Dictionary[Key] = Values is TContainer asContainer ? asContainer : NewFactory.Invoke(Values);
        }
        public static void AddValues<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, TKey Key, IEnumerable<TValue> Values) where TKey : notnull where TContainer : ICollection<TValue>, new() => Dictionary.AddValues(Key, Values, InitContainer<TValue, TContainer >());

        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<IGrouping<TKey, TValue>> ValuesToAdd, Func<IEnumerable<TValue>, TContainer> NewFactory) where TKey : notnull where TContainer : ICollection<TValue>
        {
            switch (Dictionary)
            {
                case ConcurrentDictionary<TKey, TContainer> Concurrent:
                    Parallel.ForEach(ValuesToAdd, Group => Concurrent.AddOrUpdate(Group.Key, _ => NewFactory.Invoke(Group.AsEnumerable()), (_, Collection) => { Collection.AddRange(Group.AsEnumerable()); return Collection; }));
                    break;
                default:
                    foreach (IGrouping<TKey, TValue> grouping in ValuesToAdd)
                        Dictionary.AddValues(grouping.Key, grouping.AsEnumerable(), NewFactory);
                    break;
            }
        }
        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<IGrouping<TKey, TValue>> ValuesToAdd) where TKey : notnull where TContainer : ICollection<TValue>, new() => Dictionary.AddMany(ValuesToAdd, InitContainer<TValue, TContainer>());

        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<(TKey Key, IEnumerable<TValue> Values)> ValuesToAdd, Func<IEnumerable<TValue>, TContainer> NewFactory) where TKey : notnull where TContainer : ICollection<TValue> => Dictionary.AddMany(ValuesToAdd.GroupBy(x => x.Key, x => x.Values).Select(x => (x.Key, x.SelectMany(i => i))), NewFactory);
        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<(TKey Key, IEnumerable<TValue> Values)> ValuesToAdd) where TKey : notnull where TContainer : ICollection<TValue>, new() => Dictionary.AddMany(ValuesToAdd, InitContainer<TValue, TContainer>());

        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<(TKey Key, TValue Value)> ValuesToAdd, Func<IEnumerable<TValue>, TContainer> NewFactory) where TKey : notnull where TContainer : ICollection<TValue> => Dictionary.AddMany(ValuesToAdd.GroupBy(x => x.Key, x => x.Value).Select(x => (x.Key, x.AsEnumerable())), NewFactory);
        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<(TKey Key, TValue Value)> ValuesToAdd) where TKey : notnull where TContainer : ICollection<TValue>, new() => Dictionary.AddMany(ValuesToAdd, InitContainer<TValue, TContainer>());

        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<KeyValuePair<TKey, TContainer>> ValuesToAdd) where TKey : notnull where TContainer : ICollection<TValue>
        {
            switch (Dictionary)
            {
                case ConcurrentDictionary<TKey, TContainer> Concurrent:
                        Parallel.ForEach(ValuesToAdd, Group => Concurrent.AddOrUpdate(Group.Key, _ => Group.Value, (_, Collection) => { Collection.AddRange(Group.Value); return Collection; }));
                        break;
                default:
                    foreach (KeyValuePair<TKey, TContainer> Pair in ValuesToAdd)
                    {
                        try
                        {
                            Dictionary[Pair.Key].AddRange(Pair.Value);
                        }
                        catch (KeyNotFoundException)
                        {
                            Dictionary[Pair.Key] = Pair.Value;
                        }
                    }
                        break;
            }
        }
        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<KeyValuePair<TKey, IEnumerable<TValue>>> ValuesToAdd, Func<IEnumerable<TValue>, TContainer> NewFactory) where TKey : notnull where TContainer : ICollection<TValue>
        {
            switch (Dictionary)
            {
                case ConcurrentDictionary<TKey, TContainer> Concurrent:
                        Parallel.ForEach(ValuesToAdd, Group => Concurrent.AddOrUpdate(Group.Key, _ => NewFactory.Invoke(Group.Value), (_, Collection) => { Collection.AddRange(Group.Value); return Collection; }));
                        break;
                default:
                    foreach (KeyValuePair<TKey, IEnumerable<TValue>> Pair in ValuesToAdd)
                    {
                        try
                        {
                            Dictionary[Pair.Key].AddRange(Pair.Value);
                        }
                        catch (KeyNotFoundException)
                        {
                            Dictionary[Pair.Key] = Pair.Value is TContainer asContainer ? asContainer : NewFactory.Invoke(Pair.Value);
                        }
                    }
                        break;
            }
        }
        public static void AddMany<TKey, TContainer, TValue>(this IDictionary<TKey, TContainer> Dictionary, IEnumerable<KeyValuePair<TKey, IEnumerable<TValue>>> ValuesToAdd) where TKey : notnull where TContainer : ICollection<TValue>, new() => Dictionary.AddMany(ValuesToAdd, InitContainer<TValue, TContainer>());
    }
}