﻿using System;
using System.Threading.Tasks;

namespace Qib.Extensions
{
    public static class ValueTaskContinuationExtensions
    {
        public static ValueTask<TFinalResult> ContinueAfter<TSourceResult, TFinalResult>(this ValueTask<TSourceResult> Source, Func<TSourceResult, TFinalResult> Method) => Source.IsCompleted ? ValueTask.FromResult(Method.Invoke(Source.Result)) : new(Source.AsTask().ContinueWith(T => Method.Invoke(T.Result)));
        public static Task<TFinalResult> ContinueAfter<TSourceResult, TFinalResult>(this ValueTask<TSourceResult> Source, Func<TSourceResult, Task<TFinalResult>> Method) => Source.IsCompleted ? Method.Invoke(Source.Result) : Source.AsTask().ContinueWith(T => Method.Invoke(T.Result)).Unwrap();
        public static ValueTask<TFinalResult> ContinueAfter<TSourceResult, TFinalResult>(this ValueTask<TSourceResult> Source, Func<TSourceResult, ValueTask<TFinalResult>> Method) => Source.IsCompleted ? Method.Invoke(Source.Result) : new(Source.AsTask().ContinueWith(T => Method.Invoke(T.Result)).Unwrap());
    }
}