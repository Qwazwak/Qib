﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Qib.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> Source) => Source.Shuffle(DateTime.UtcNow.Millisecond * DateTime.UtcNow.Second);
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> Source, int Seed) => Source.Shuffle(new Random(Seed));
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> Source, Random rng)
        {
            if (Source is ICollection<T> Collection)
            {
                Collection.Shuffle(rng);
                return Collection;
            }
            return Source.OrderBy(_ => rng.Next());
        }

        public static IEnumerable<IEnumerable<T>> Powerset<T>(this IEnumerable<T> Source)
        {
            IEnumerable<IEnumerable<T>> Seed = Enumerable.Empty<IEnumerable<T>>().Append(Enumerable.Empty<T>());
            return Source.Aggregate(Seed, (Acc, i) => Acc = Acc.Concat(Acc.Select(j => j.Append(i))));
        }

        public static IEnumerable<IEnumerable<T>> PowersetShuffle<T>(this IEnumerable<T> Source) => Source.Powerset().Select(i => i.Shuffle()).Shuffle();

        #region Counting

        private static long ParallelCountInternal<T>(IEnumerable<T> source)
        {
            long Counter = 0;
            Parallel.ForEach(source, _ => Interlocked.Increment(ref Counter));
            return Counter;
        }

        public static long ParallelCount<T>(this IEnumerable<T> source) => source switch
        {
            ICollection<T> TCollection => TCollection.Count,
            ICollection Collection => Collection.Count,
            _ => ParallelCountInternal(source)
        };

        public static ValueTask<long> ParallelCountAsync<T>(this IEnumerable<T> source) => source switch
        {
            ICollection<T> TCollection => ValueTask.FromResult((long)TCollection.Count),
            ICollection Collection => ValueTask.FromResult((long)Collection.Count),
            _ => new(Task<long>.Factory.StartNew(() => ParallelCountInternal(source)))
        };

        public static bool None<T>(this IEnumerable<T> source) => !source.Any();
        public static bool None<T>(this IEnumerable<T> source, Func<T, bool> Predicate) => !source.Any(Predicate);

        public static bool MoreThan<TSource>(this IEnumerable<TSource> source, int MinimumToContain) => source.Atleast(MinimumToContain + 1);
        public static bool Atleast<TSource>(this IEnumerable<TSource> source, int MinimumToContain) => source switch
        {
            ICollection<TSource> collectionT => collectionT.Count >= MinimumToContain,
            ICollection collection => collection.Count >= MinimumToContain,
            _ => source.Skip(MinimumToContain - 1).Any(),
        };
        public static bool LessThan<TSource>(this IEnumerable<TSource> source, int MaximumToContain) => source.AtMost(MaximumToContain - 1);
        public static bool AtMost<TSource>(this IEnumerable<TSource> Source, int MaximumToContain) => Source switch
        {
            ICollection<TSource> collectionT => collectionT.Count <= MaximumToContain,
            ICollection collection => collection.Count <= MaximumToContain,
            _ => !Source.Skip(MaximumToContain).Any()
        };

        #endregion Counting

        #region Helper Wrappers

        public static IEnumerable<(int Index, TSource Value)> WithIndex<TSource>(this IEnumerable<TSource> Source) => Source.Select((i, index) => (Index: index, Value: i));
        public static IEnumerable<TSource> Repeat<TSource>(this IEnumerable<TSource> Source)
        {
            while (true)
            {
                foreach (TSource item in Source)
                {
                    yield return item;
                }
            }
        }

        public static IEnumerable<(TSource, TOther)> ModWithArray<TSource, TOther>(this IEnumerable<TSource> Enumerable, TOther[] Other) => Enumerable.Select((i, index) => (i, Other[index % Other.Length]));
        public static IEnumerable<(TSource, TOther)> ModWith<TSource, TOther>(this IEnumerable<TSource> Enumerable, params TOther[] Other) => Enumerable.ModWith(Other.AsEnumerable());
        public static IEnumerable<(TSource, TOther)> ModWith<TSource, TOther>(this IEnumerable<TSource> Enumerable, IEnumerable<TOther> Other) => Enumerable.Zip(Other.Repeat());

        #endregion Helper Wrappers

        #region Searching

        public static bool TryFirst<TSource>(this IEnumerable<TSource>? Source, out TSource Result)
        {
            try
            {
                Result = Source!.First();
                return true;
            }
            catch
            {
                Result = default!;
                return false;
            }
        }

        public static bool TryFirst<TSource>(this IEnumerable<TSource>? Source, Func<TSource, bool> Selector, out TSource Result) => (Source?.Where(Selector)).TryFirst(out Result);
        public static bool TryLast<TSource>(this IEnumerable<TSource>? Source, out TSource Result) => (Source?.Reverse()).TryFirst(out Result);
        public static bool TryLast<TSource>(this IEnumerable<TSource>? Source, Func<TSource, bool> Selector, out TSource Result) => (Source?.Reverse()).TryFirst(Selector, out Result);

        public static TSource? FirstOrNull<TSource>(this IEnumerable<TSource>? Source) => Source.TryFirst(out TSource Result) ? Result : default;
        public static TSource? FirstOrNull<TSource>(this IEnumerable<TSource>? Source, Func<TSource, bool> Selector) => Source.TryFirst(Selector, out TSource? Result) ? Result : default;

        public static TSource? LastOrNull<TSource>(this IEnumerable<TSource>? Source) => (Source?.Reverse()).FirstOrNull();
        public static TSource? LastOrNull<TSource>(this IEnumerable<TSource>? Source, Func<TSource, bool> Selector) => (Source?.Reverse()).FirstOrNull(Selector);

        #endregion Searching

        #region Filtering

        public static IEnumerable<TSource> WhereNotNull<TSource>(this IEnumerable<TSource?> Enumerable) => Enumerable.Where(i => i != null).Select(i => i!);
        public static IEnumerable<TSource> WhereNotNull<TSource, TInner>(this IEnumerable<TSource> Enumerable, Func<TSource, TInner?> Accessor) => Enumerable.Where(i => i != null && Accessor.Invoke(i) != null).Select(i => i!);
        public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> source, TSource ItemToExclude) => source.Except(ItemToExclude, EqualityComparer<TSource>.Default);
        public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> source, TSource ItemToExclude, IEqualityComparer<TSource> Comparer) => source.Where(i => Comparer.Equals(i, ItemToExclude));

        public static bool AllBoolEqual(this IEnumerable<bool> source, bool ValueToMatch) => source.Contains(ValueToMatch);
        public static bool AllTrue(this IEnumerable<bool> source) => source.Contains(true);
        public static bool AllFalse(this IEnumerable<bool> source) => source.Contains(false);

        public static bool AllBoolEqual<T>(this IEnumerable<T> source, Func<T, bool> Accessor, bool ValueToMatch) => source.Select(Accessor).Contains(ValueToMatch);
        public static bool AllTrue<T>(this IEnumerable<T> source, Func<T, bool> Accessor) => source.Select(Accessor).Contains(true);
        public static bool AllFalse<T>(this IEnumerable<T> source, Func<T, bool> Accessor) => source.Select(Accessor).Contains(false);

        #endregion Filtering
    }
}