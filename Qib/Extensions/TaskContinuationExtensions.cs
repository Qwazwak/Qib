﻿using System;
using System.Threading.Tasks;

namespace Qib.Extensions
{
    public static class TaskContinuationExtensions
    {
        public static Task<TFinalResult> ContinueAfter<TSourceResult, TFinalResult>(this Task<TSourceResult> Source, Func<TSourceResult, TFinalResult> Method) => Source.ContinueWith(T => Method.Invoke(T.Result));
        public static Task<TFinalResult> ContinueAfter<TSourceResult, TFinalResult>(this Task<TSourceResult> Source, Func<TSourceResult, Task<TFinalResult>> Method) => Source.ContinueWith(T => Method.Invoke(T.Result)).Unwrap();
        public static Task<TFinalResult> ContinueAfter<TSourceResult, TFinalResult>(this Task<TSourceResult> Source, Func<TSourceResult, ValueTask<TFinalResult>> Method) => Source.ContinueWith(T => Method.Invoke(T.Result)).Unwrap();
        public static Task ContinueAfter<TSourceResult>(this Task<TSourceResult> Source, Action<TSourceResult> Method) => Source.ContinueWith(T => Method.Invoke(T.Result));
    }
}