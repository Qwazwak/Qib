﻿using System.Collections;
using System.Collections.Generic;

namespace Qib.Extensions
{
    public static class IEnumerablePlainExtensions
    {
        public static IEnumerable<T> AsTyped<T>(this IEnumerable Source)
        {
            foreach (object item in Source)
            {
                yield return (T)item;
            }
        }
    }
}