﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Qib.Extensions
{
    public static class IAsyncEnumerableGeneratorExtentions
    {
        public static IAsyncEnumerable<TResult> SelectAsync<TSource, TResult>(this IEnumerable<TSource> Source, Func<TSource, ValueTask<TResult>> AsyncAction) => Source.Select(AsyncAction).SelectAsync();
        public static IAsyncEnumerable<TResult> SelectAsync<TSource, TResult>(this IEnumerable<TSource> Source, Func<TSource, Task<TResult>> AsyncAction) => Source.Select(AsyncAction).SelectAsync();

        public static IAsyncEnumerable<TResult> SelectAsync<TResult>(this IEnumerable<ValueTask<TResult>> Source)
        {
            IEnumerable<IGrouping<bool, ValueTask<TResult>>> BaseEnumerable = Source.GroupBy(i => i.IsCompleted);
            return BaseEnumerable.Where(i => i.Key).SelectMany(i => i.Select(j => j.Result)).ToAsyncEnumerable().Concat(SelectAsync(BaseEnumerable.Where(i => !i.Key).SelectMany(i => i.Select(j => j.AsTask()))));
        }

        public static async IAsyncEnumerable<TResult> SelectAsync<TResult>(this IEnumerable<Task<TResult>> Source)
        {
            foreach (Task<TResult> item in Source)
            {
                item.Start();
            }
            IEnumerable<Task<TResult>> RemainingWork = Source;
            IEnumerable<Task<TResult>> CompletedTasks = Enumerable.Empty<Task<TResult>>();

            while (RemainingWork.Any())
            {
                yield return await Task.WhenAny(RemainingWork).ContinueAfter(LatestCompleted =>
                {
                    CompletedTasks = CompletedTasks.Append(LatestCompleted);

                    RemainingWork = Source.Except(CompletedTasks);
                    return LatestCompleted;
                });
            }
        }
    }
}