﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Qib.Tools
{
    public class RSA_PublicKey
    {
        private RSAParameters RSAPublicParameters;

        public int KeySize { get; private init; }

        public ReadOnlySpan<byte> Exponent => new(RSAPublicParameters.Exponent);
        public ReadOnlySpan<byte> Modulus => new(RSAPublicParameters.Modulus);

        public RSA_PublicKey(int KeySize, ReadOnlySpan<byte> Exponent, ReadOnlySpan<byte> Modulus) : this(KeySize, Exponent.ToArray(), Modulus.ToArray()) { }

        public RSA_PublicKey(int KeySize, byte[] Exponent, byte[] Modulus)
        {
            this.KeySize = KeySize;
            RSAPublicParameters = new RSAParameters(){ Exponent = Exponent, Modulus = Modulus };
        }

        public Task<bool> VerifySignatureFromSourceAsync(Stream OriginalData, byte[] Signature, HashType HashAlgorithm, CancellationToken cancellationToken = default)
            => HashCalculator.CreateAsync(HashAlgorithm, OriginalData, cancellationToken).ContinueWith(t => VerifySignatureFromSourceAsync(t.Result, Signature, HashAlgorithm, cancellationToken)).Unwrap();

        public Task<bool> VerifySignatureFromSourceAsync(byte[] OriginalData, byte[] Signature, HashType HashAlgorithm, CancellationToken cancellationToken = default)
            => HashCalculator.CreateAsync(HashAlgorithm, OriginalData, cancellationToken).ContinueWith(t => VerifySignatureFromSourceAsync(t, Signature, HashAlgorithm, cancellationToken)).Unwrap();

        private Task<bool> VerifySignatureFromSourceAsync(Task<byte[]> Hash, byte[] Signature, HashType HashAlgorithm, CancellationToken cancellationToken = default)
            => Hash.ContinueWith(t => !cancellationToken.IsCancellationRequested && VerifySignature(t.Result, Signature, HashAlgorithm));

        public bool VerifySignatureFromSource(byte[] OriginalData, byte[] Signature, HashType HashAlgorithm) => VerifySignature(HashCalculator.Create(HashAlgorithm, OriginalData), Signature, HashAlgorithm);

        /// <summary>
        /// Verify a signature
        /// </summary>
        /// <param name="GivenHash">Hash of data to verify</param>
        /// <param name="Signature">Given signature to verify</param>
        /// <param name="HashAlgorithm">Type of hashing used</param>
        /// <returns>True if the signature matches</returns>
        public bool VerifySignature(byte[] GivenHash, byte[] Signature, HashType HashAlgorithm)
        {
            using RSACryptoServiceProvider rsa = new(KeySize);
            rsa.ImportParameters(RSAPublicParameters);
            RSAPKCS1SignatureDeformatter rsaDeformatter = new(rsa);
            rsaDeformatter.SetHashAlgorithm(HashTypeToString(HashAlgorithm));
            return rsaDeformatter.VerifySignature(GivenHash, Signature);
        }

        protected static string HashTypeToString(HashType Hash) => Hash switch
        {
            HashType.SHA1 => "SHA1",
            HashType.SHA256 => "SHA256",
            HashType.SHA384 => "SHA384",
            HashType.SHA512 => "SHA512",
            _ => throw new ArgumentOutOfRangeException(nameof(Hash)),
        };
    }
}
