﻿using Qib.Extensions;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Qib.Tools
{
    public static class HashCalculator
    {
        private static readonly Lazy<SHA1> Sha1 = new(SHA1.Create, true);
        private static readonly Lazy<SHA256> Sha256 = new(SHA256.Create, true);
        private static readonly Lazy<SHA384> Sha384 = new(SHA384.Create, true);
        private static readonly Lazy<SHA512> Sha512 = new(SHA512.Create, true);

        /// <summary>
        ///     Creates a hash and retuns the byte array of the hash
        /// </summary>
        /// <param name="HashType">The type of hash algorithm to use</param>
        /// <param name="data">The data to hash.</param>
        /// <param name="cancellationToken">Token for early cancellation</param>
        public static Task<byte[]> CreateAsync(HashType HashType, byte[] data, CancellationToken cancellationToken = default)
        {
            ArgumentNullException.ThrowIfNull(HashType);
            ArgumentNullException.ThrowIfNull(data);
            MemoryStream Reader = new(data);
            return GetAlgorithm(HashType).ComputeHashAsync(Reader, cancellationToken).ContinueWith(B =>
            {
                B.Wait(cancellationToken);
                Reader.Dispose();
                return B.Result;
            });
        }

        /// <summary>
        ///     Creates a hash and retuns the byte array of the hash
        /// </summary>
        /// <param name="HashType">The type of hash algorithm to use</param>
        /// <param name="data">The data to hash.</param>
        /// <param name="cancellationToken">Token for early cancellation</param>
        public static Task<byte[]> CreateAsync(HashType HashType, Stream data, CancellationToken cancellationToken = default)
        {
            ArgumentNullException.ThrowIfNull(HashType);
            ArgumentNullException.ThrowIfNull(data);
            return GetAlgorithm(HashType).ComputeHashAsync(data, cancellationToken);
        }

        /// <summary>
        ///     Creates a hash and retuns the byte array of the hash
        /// </summary>
        /// <param name="HashType">The type of hash algorithm to use</param>
        /// <param name="data">The data to hash.</param>
        public static byte[] Create(HashType HashType, byte[] data)
        {
            ArgumentNullException.ThrowIfNull(HashType);
            ArgumentNullException.ThrowIfNull(data);
            return GetAlgorithm(HashType).ComputeHash(data);
        }

        public static Task<bool> VerifyAsync(HashType HashType, byte[] data, byte[] hash, CancellationToken cancellationToken = default)
        {
            ArgumentNullException.ThrowIfNull(HashType);
            ArgumentNullException.ThrowIfNull(data);
            return CreateAsync(HashType, data, cancellationToken).ContinueWith(H => InternalEqual(hash, H.Result));
        }

        public static Task<bool> VerifyAsync(HashType HashType, Stream data, byte[] hash, CancellationToken cancellationToken = default)
        {
            ArgumentNullException.ThrowIfNull(HashType);
            ArgumentNullException.ThrowIfNull(data);
            return CreateAsync(HashType, data, cancellationToken).ContinueWith(H => InternalEqual(hash, H.Result));
        }

        public static bool Verify(HashType hashType, byte[] data, byte[] hash) => InternalEqual(hash, Create(hashType, data));
        public static bool InternalEqual(byte[] A, byte[] B) => A == B && A != null && B != null && A.Length == B.Length && A.Zip(B).AllTrue(p => p.First == p.Second);

        private static HashAlgorithm GetAlgorithm(HashType HashAlgorithm) => HashAlgorithm switch
        {
            HashType.SHA1 => Sha1.Value,
            HashType.SHA256 => Sha256.Value,
            HashType.SHA384 => Sha384.Value,
            HashType.SHA512 => Sha512.Value,
            _ => throw new ArgumentOutOfRangeException(nameof(HashAlgorithm)),
        };
    }
}
