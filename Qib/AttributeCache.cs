﻿using Qib.Extensions;
using System;
using System.Collections.Concurrent;
using System.Reflection;

namespace Qib
{
    public static class AttributeCacheUntyped
    {
        private static readonly ConcurrentDictionary<ICustomAttributeProvider, Attribute[]> PlainCache = new();
        private static readonly ConcurrentDictionary<ICustomAttributeProvider, Attribute[]> InheritCache = new();

        public static Attribute[] GetPlain(ICustomAttributeProvider T) => PlainCache.GetOrAdd(T, k => (Attribute[])k.GetCustomAttributes(false));
        public static Attribute[] GetInherited(ICustomAttributeProvider T) => InheritCache.GetOrAdd(T, k => (Attribute[])k.GetCustomAttributes(true));
        public static Attribute[] Get(ICustomAttributeProvider T, bool Inherited) => Inherited ? GetInherited(T) : GetPlain(T);
        public static (Attribute[] PlainAttributes, Attribute[] InheritedAttributes) GetBoth(ICustomAttributeProvider T) => (GetPlain(T), GetInherited(T));
        public static Attribute[] GetPlain<T>() => PlainCache.GetOrAdd(typeof(T), k => (Attribute[])k.GetCustomAttributes(false));
        public static Attribute[] GetInherited<T>() => InheritCache.GetOrAdd(typeof(T), k => (Attribute[])k.GetCustomAttributes(true));
        public static Attribute[] Get<T>(bool Inherited) => Inherited ? GetInherited<T>() : GetPlain<T>();
        public static (Attribute[] PlainAttributes, Attribute[] InheritedAttributes) GetBoth<T>() => GetBoth(typeof(T));
    }

    public static class AttributeCache {
        private static readonly ConcurrentDictionary<(ICustomAttributeProvider Provider, Type AttributeType), Attribute[]> PlainCacheTyped = new();
        private static readonly ConcurrentDictionary<(ICustomAttributeProvider Provider, Type AttributeType), Attribute[]> InheritCacheTyped = new();

        public static TAttribute[] GetPlain<TAttribute>(ICustomAttributeProvider T) where TAttribute : Attribute => (TAttribute[])PlainCacheTyped.GetOrAdd((T, typeof(TAttribute)), k => (TAttribute[])k.Provider.GetCustomAttributes(k.AttributeType, false));
        public static TAttribute[] GetInherited<TAttribute>(ICustomAttributeProvider T) where TAttribute : Attribute => (TAttribute[])InheritCacheTyped.GetOrAdd((T, typeof(TAttribute)), k => (TAttribute[])k.Provider.GetCustomAttributes(k.AttributeType, true));
        public static TAttribute[] Get<TAttribute>(ICustomAttributeProvider T, bool Inherited) where TAttribute : Attribute => Inherited ? GetInherited<TAttribute>(T) : GetPlain<TAttribute>(T);
        public static (TAttribute[] PlainAttributes, TAttribute[] InheritedAttributes) GetBoth<TAttribute>(ICustomAttributeProvider T) where TAttribute : Attribute => (GetPlain<TAttribute>(T), GetInherited<TAttribute>(T));
        public static TAttribute[] GetPlain<T, TAttribute>() where TAttribute : Attribute => GetPlain<TAttribute>(typeof(T));
        public static TAttribute[] GetInherited<T, TAttribute>() where TAttribute : Attribute => GetInherited<TAttribute>(typeof(T));
        public static TAttribute[] Get<T, TAttribute>(bool Inherited) where TAttribute : Attribute => Inherited ? GetInherited<TAttribute>(typeof(T)) : GetPlain<TAttribute>(typeof(T));
        public static (TAttribute[] PlainAttributes, TAttribute[] InheritedAttributes) GetBoth<T, TAttribute>() where TAttribute : Attribute => GetBoth<TAttribute>(typeof(T));

        public static bool HasAttribute<TAttribute>(ICustomAttributeProvider T, bool Inherited) where TAttribute : Attribute => Get<TAttribute>(T, Inherited).Length > 0;
        public static bool HasAttributePlain<TAttribute>(ICustomAttributeProvider T) where TAttribute : Attribute => GetPlain<TAttribute>(T).Length > 0;
        public static bool HasAttributeInherited<TAttribute>(ICustomAttributeProvider T) where TAttribute : Attribute => GetInherited<TAttribute>(T).Length > 0;
        public static bool HasAttribute<T, TAttribute>(bool Inherited) where TAttribute : Attribute => Get<T, TAttribute>(Inherited).Length > 0;
        public static bool HasAttributePlain<T, TAttribute>() where TAttribute : Attribute => GetPlain<T, TAttribute>().Length > 0;
        public static bool HasAttributeInherited<T, TAttribute>() where TAttribute : Attribute => GetInherited<T, TAttribute>().Length > 0;
        
        public static bool TryGetAttributes<TAttribute>(ICustomAttributeProvider T, bool Inherited, out TAttribute[] Result) where TAttribute : Attribute => (Result = Get<TAttribute>(T, Inherited)).Length > 0;
        public static bool TryGetPlainAttributes<TAttribute>(ICustomAttributeProvider T, out TAttribute[] Result) where TAttribute : Attribute => (Result = GetPlain<TAttribute>(T)).Length > 0;
        public static bool TryGetInheritedAttributes<TAttribute>(ICustomAttributeProvider T, out TAttribute[] Result) where TAttribute : Attribute => (Result = GetInherited<TAttribute>(T)).Length > 0;
        public static bool TryGetAttributes<T, TAttribute>(bool Inherited, out TAttribute[] Result) where TAttribute : Attribute => (Result = Get<T, TAttribute>(Inherited)).Length > 0;
        public static bool TryGetPlainAttributes<T, TAttribute>(out TAttribute[] Result) where TAttribute : Attribute => (Result = GetPlain<T, TAttribute>()).Length > 0;
        public static bool TryGetInheritedAttributes<T, TAttribute>(out TAttribute[] Result) where TAttribute : Attribute => (Result = GetInherited<T, TAttribute>()).Length > 0;

        public static bool TryGetAttribute<TAttribute>(ICustomAttributeProvider T, bool Inherited, out TAttribute Result) where TAttribute : Attribute => Get<TAttribute>(T, Inherited).TryFirst(out Result);
        public static bool TryGetPlainAttribute<TAttribute>(ICustomAttributeProvider T, out TAttribute Result) where TAttribute : Attribute => GetPlain<TAttribute>(T).TryFirst(out Result);
        public static bool TryGetInheritedAttribute<TAttribute>(ICustomAttributeProvider T, out TAttribute Result) where TAttribute : Attribute => GetInherited<TAttribute>(T).TryFirst(out Result);
        public static bool TryGetAttribute<T, TAttribute>(bool Inherited, out TAttribute Result) where TAttribute : Attribute => Get<TAttribute>(typeof(T), Inherited).TryFirst(out Result);
        public static bool TryGetPlainAttribute<T, TAttribute>(out TAttribute Result) where TAttribute : Attribute => GetPlain<TAttribute>(typeof(T)).TryFirst(out Result);
        public static bool TryGetInheritedAttribute<T, TAttribute>(out TAttribute Result) where TAttribute : Attribute => GetInherited<TAttribute>(typeof(T)).TryFirst(out Result);
    }
}