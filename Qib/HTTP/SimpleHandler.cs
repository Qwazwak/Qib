﻿using Qib.Extensions;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Qib.HTTP
{
    public abstract class SimpleHandler : DelegatingHandler
    {
        protected SimpleHandler() { }
        protected SimpleHandler(DelegatingHandler InnerHandler) : base(InnerHandler) { }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage Request, CancellationToken CancellationToken) => BeforeAsync(Request, CancellationToken).ContinueAfter(Request => base.SendAsync(Request, CancellationToken)).ContinueAfter(Response => AfterAsync(Response, CancellationToken));
        protected override HttpResponseMessage Send(HttpRequestMessage Request, CancellationToken CancellationToken) => After(base.Send(Before(Request, CancellationToken), CancellationToken), CancellationToken);

        protected virtual HttpRequestMessage Before(HttpRequestMessage Request, CancellationToken CancellationToken) => Request;
        protected virtual HttpResponseMessage After(HttpResponseMessage Response, CancellationToken CancellationToken) => Response;

        protected virtual ValueTask<HttpRequestMessage> BeforeAsync(HttpRequestMessage Request, CancellationToken CancellationToken) => ValueTask.FromResult(Request);
        protected virtual ValueTask<HttpResponseMessage> AfterAsync(HttpResponseMessage Response, CancellationToken CancellationToken) => ValueTask.FromResult(Response);
    }
}