﻿using Qib.Extensions;
using Qib.Tools;
using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Qib.HTTP
{
    public abstract class HashSignHandler : SimpleHandler
    {
        private const HashType DefaultAlgorithm = HashType.SHA256;
        private readonly RSA_PrivateKey SigningKey;
        private readonly HashType HashAlgorithm;
        private readonly string HeaderKey;

        protected HashSignHandler(string HeaderKey, RSA_PrivateKey SigningKey, HashType HashAlgorithm = DefaultAlgorithm)
        {
            this.SigningKey = SigningKey;
            this.HashAlgorithm = HashAlgorithm;
            this.HeaderKey = HeaderKey;
        }

        protected HashSignHandler(string HeaderKey, RSA_PrivateKey SigningKey, DelegatingHandler InnerHandler, HashType HashAlgorithm = DefaultAlgorithm) : base(InnerHandler)
        {
            this.SigningKey = SigningKey;
            this.HashAlgorithm = HashAlgorithm;
            this.HeaderKey = HeaderKey;
        }

        protected override HttpRequestMessage Before(HttpRequestMessage Request, CancellationToken CancellationToken)
        {
            if (Request.Content != null)
            {
                using MemoryStream CopyStream = new();
                Request.Content.ReadAsStream(CancellationToken).CopyTo(CopyStream);
                Request.Headers.Add(HeaderKey, Convert.ToBase64String(SigningKey.SignData(CopyStream.ToArray(), HashAlgorithm)));
            }

            return Request;
        }

        protected override ValueTask<HttpRequestMessage> BeforeAsync(HttpRequestMessage Request, CancellationToken CancellationToken)
        {
            if(Request.Content != null)
                return new(Request.Content.ReadAsStreamAsync(CancellationToken).ContinueAfter(s => SigningKey.SignDataAsync(s, HashAlgorithm, CancellationToken)).ContinueAfter(r => { Request.Headers.Add(HeaderKey, Convert.ToBase64String(r)); return Request; }));
            return ValueTask.FromResult(Request);
        }
    }
}