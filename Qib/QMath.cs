﻿namespace Qib
{
    public static class QMath
    {
        public static bool IsEven(int Source) => Source % 2 == 0;
        public static bool IsOdd(int Source) => Source % 2 == 1;
        public static int CountDigits(int Source) => Source == 0 ? 1 : (int)System.Math.Floor(System.Math.Log10(System.Math.Abs(Source)) + 1);
        public static long CountDigits(long Source) => Source == 0 ? 1 : (long)System.Math.Floor(System.Math.Log10(System.Math.Abs(Source)) + 1);
    }
}
