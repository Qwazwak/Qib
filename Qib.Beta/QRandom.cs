﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace Qib.Beta
{
    internal static class QRandom
    {
        public static int DefaultBufferSize = 128;
        private static readonly RandomNumberGenerator RNG = RandomNumberGenerator.Create();

        public static IEnumerable<byte> RandomBytes(int NumberOfBytes, [Optional] int? BufferSize) => EndlessRandomBytes(BufferSize).Take(NumberOfBytes);

        public static IEnumerable<byte> EndlessRandomBytes([Optional] int? BufferSize) => EndlessRandomBytesChunked(BufferSize).SelectMany(i => i);
        public static IEnumerable<byte[]> EndlessRandomBytesChunked([Optional] int? BufferSize)
        {
            // Guid.NewGuid and System.Random are not particularly random. By using a
            // cryptographically-secure random number generator, the caller is always
            // protected, regardless of use.
            byte[] buf = new byte[BufferSize.HasValue && BufferSize > 0 ? BufferSize.Value : DefaultBufferSize];
            while (true)
            {
                RNG.GetBytes(buf);
                yield return buf;
            }
        }
    }
}
