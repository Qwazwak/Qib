﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Qib.Beta
{
    public class EnumerableOnlyCollection<T> :  ICollection<T>
    {
        protected int? InternalCount { get; private set; }
        protected IEnumerable<T> Internal;
        protected readonly IEqualityComparer<T> Comparer;

        public EnumerableOnlyCollection() : this(Enumerable.Empty<T>(), EqualityComparer<T>.Default, 0) { }
        public EnumerableOnlyCollection(IEnumerable<T> Original) : this(Original, EqualityComparer<T>.Default, null) { }
        public EnumerableOnlyCollection(IEqualityComparer<T> Comparer) : this(Enumerable.Empty<T>(), Comparer, 0) { }
        public EnumerableOnlyCollection(IEnumerable<T> Original, IEqualityComparer<T> Comparer) : this(Original, Comparer, null) { }
        public EnumerableOnlyCollection(IEnumerable<T> Original, IEqualityComparer<T> Comparer, int? Count = null)
        {
            Internal = Original;
            InternalCount = Count;
            this.Comparer = Comparer;
        }

        public IEnumerator<T> GetEnumerator() => Internal.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)Internal).GetEnumerator();
        public int Count => InternalCount ??= Internal.Count();

        public virtual bool IsReadOnly => false;

        public void Add(T item)
        {
            if (InternalCount.HasValue)
                InternalCount++;
            Internal = Internal.Append(item);
        }

        public void Clear()
        {
            InternalCount = 0;
            Internal = Enumerable.Empty<T>();
        }

        public bool Contains(T item) => Internal.Contains(item);
        public void CopyTo(T[] array, int arrayIndex) => Internal.ToArray().CopyTo(array, arrayIndex);
        public bool Remove(T item)
        {
            if (!Contains(item))
                return false;
            Internal = Internal.Where(i => Comparer.Equals(i, item));

            if (InternalCount.HasValue)
                InternalCount--;
            return true;
        }
    }
}
